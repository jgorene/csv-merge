// création simple de table html pour sweetALert pvtTable
const createTableColumns = (headers, data, className, table, action) => {
    // console.log(headers, data, className, table, action);
    let input0 = '<input type="checkbox" style="width:15px; opacity: 1; height: 1.5em; margin-top: -0.7em" id="checkAll"/>';
    let html = document.createElement("div"),
        p = document.createElement("p"),
        title = "",
        text = action !== "group" ? "" : "3 colonnes max",
        buttons = (action === "hide") ?
            { hide: "masquer", annuler: true } : (action === "show") ?
                { show: "afficher", annuler: true } : { group: "grouper", annuler: true };

    // console.log(action, buttons);

    let tab = '<table class="' + className + ' tableForSweet" style="margin:5px auto">';
    tab += '<thead><tr>';
    headers.forEach((header, i) => {
        if (i === 0) {
            tab += '<th id="th_' + i + '">' + input0 + '</th>';
        } else {
            tab += '<th id="th_' + i + '">' + header + '</th>';
        }
    });
    tab += '</tr></thead>';
    tab += '<tbody>';
    data.forEach((name, i) => {
        tab += '<tr>';
        tab += '<td><input type="checkbox" style="width:15px; opacity: 1; height: 1.5em" class="checkboxColumn" value="' + name + '"/></td>';
        tab += '<td>' + name + '</td>';
        tab += '</tr>';
    });
    tab += '</tbody></table>';

    // Réinitialise le premier <td> dans les titres par un <th>
    // $('table.pvtTable thead tr th:first-child').prepend('<input type="checkbox" style="width:15px; opacity: 1; height: 1.5em" class="checkboxColumn" value="all"/>');

    // console.log($.parseHTML(extraTable)[0])
    // html.appendChild($.parseHTML(tab)[0]);
    const parser = new DOMParser();
    const elementTable = parser.parseFromString(tab, "text/html").querySelector('table');
    html.appendChild(elementTable);

    setTimeout(() => {
        if (!buttons.group) {
            document.getElementById('checkAll').addEventListener('click', function (e) {
                // console.log(this.checked);
                document.querySelectorAll('.checkboxColumn')
                    .forEach(checkbox => checkbox.checked = (this.checked) ? true : false);
            }, false);
        } else {
            document.getElementById('checkAll').disabled = true;
        }

    }, 10);

    swal({
        title: title,
        text: text,
        content: html,
        className: "sweetalert-auto",
        buttons: buttons,
    })
        .then((value) => {
            console.log(value);
            let inputElements;
            switch (value) {
                case "hide":
                    inputElements = document.getElementsByClassName('checkboxColumn');
                    for (let i = 0, lgi = inputElements.length; i < lgi; ++i) {
                        // console.log(inputElements[i].value);
                        if (inputElements[i].checked) {
                            table.hideColumn(inputElements[i].value);
                        }
                    }
                    swal("Terminé !", "", "success");
                    break;

                case "show":
                    inputElements = document.getElementsByClassName('checkboxColumn');
                    for (let i = 0, lgi = inputElements.length; i < lgi; ++i) {
                        if (inputElements[i].checked) {
                            table.showColumn(inputElements[i].value);
                        }
                    }
                    swal("Terminé !", "", "success");
                    break;

                case "group":
                    inputElements = document.getElementsByClassName('checkboxColumn');
                    let count = 0;
                    let fields = [];
                    for (let i = 0, lgi = inputElements.length; i < lgi; ++i) {
                        if (inputElements[i].checked) {
                            console.log(inputElements[i]);
                            fields.push(inputElements[i].value);
                            count++;
                        }
                    }
                    if (count < 4) {
                        console.log(count, fields);
                        groupByCol(table, fields);
                        document.getElementById('groupBy-icon').innerHTML =
                            '<i class="fas fa-lock"></i>';
                        document.getElementById('groupBy-btn').textContent = "Dégrouper";
                        // swal("Terminé !", "", "success");
                    } else {
                        swal("Maximum dépassé !",
                            "Groupement par 3 niveaux de colonnes maximum, désolé...",
                            "warning");
                    }
                    break;

                default:
                    break;
            }
        });

    // prettyDefault(title, text, html, "", "sweetalert-auto");
    return true;
}

const createTableHeaders = async (headers, data, className, divId, delimiter) => {
    // console.log(headers, data, className, divId);
    const gridDiv = document.getElementById(divId);
    gridDiv.innerHTML = "";
    
    const checkbox = (name, index) => 
    `<input 
    type="checkbox" 
    data-index="${index}" 
    style="position: relative; width:15px; opacity: 1; height: 1.5em; vertical-align: middle;" 
    class="checkboxHeader" 
    value="${name}"
    />`;

    const th = (header, i) => `
    '<th id="th_${i}"style="background-color: transparent  !important">${header}</th>')
    `;

    let tab = '<table class="' + className + '" style="margin:5px">';
    // // non utile ici
    // tab += '<thead><tr>';
    // headers.forEach((header, i) => tab += th(header, i));
    // tab += '</tr></thead>';
    tab += '<tbody>';
    data.forEach((name, i) => {
        // console.log(name.join(','));
        tab += '<tr>';
        tab += '<td>' + checkbox(name, i) + '</td>';
        tab += '<td>' + JSON.stringify(name.join(delimiter)) + '</td>';
        tab += '</tr>';
    });
    tab += '</tbody></table>';

    // Réinitialise le premier <td> dans les titres par un <th>
    // $('table.pvtTable thead tr th:first-child').prepend('<input type="checkbox" style="width:15px; opacity: 1; height: 1.5em" class="checkboxHeader" value="all"/>');

    // console.log($.parseHTML(extraTable)[0])
    // html.appendChild($.parseHTML(tab)[0]);
    const parser = new DOMParser();
    const elementTable = parser.parseFromString(tab, "text/html").querySelector('table');
    gridDiv.appendChild(elementTable);

   // prettyDefault(title, text, html, "", "sweetalert-auto");
    return elementTable;
}

const createTableExport = (headers, data, className) => {
    // console.log(data, headers);
    let html = document.createElement("div"),
        p = document.createElement("p"),
        title = "Info participant",
        text = "";
    let table = '<table class="' + className + ' tableForSweet" style="margin:5px auto">';
    table += '<thead><tr>';
    headers.forEach(header => {
        // console.log(header);
        table += '<th>' + header + '</th>';
    });
    table += '</tr></thead>';
    table += '<tbody>';
    data.forEach(row => {
        // console.log(row.length);
        table += '<tr>';
        // row.forEach(cell => {
        table += '<td>' + row[0] + '</td>';
        table += '<td>' + row[1] + '</td>';
        // })
        table += '</tr>';
    });
    table += '</tbody></table>';

    const parser = new DOMParser();
    const elementTable = parser.parseFromString(table, "text/html").querySelector('table');
    html.appendChild(elementTable);

    swal({
        title: title,
        text: text,
        content: html,
        className: "sweetalert-auto",
        buttons: {
            export: "export CSV",
            annuler: true,
        },
    })
        .then((value) => {
            switch (value) {
                case "export":
                    data.unshift(headers)
                    exportToCsv(data, "fiche-info")
                    break;

                default:
                    break;
            }
        });

    // prettyDefault(title, text, html, "", "sweetalert-auto");
    return true;
}

const headerMenu = function () {
    let menu = [];
    let columns = this.getColumns();

    for (let column of columns) {
        //create checkbox element using font awesome icons
        let icon = document.createElement("i");
        icon.classList.add("fas");
        icon.classList.add(column.isVisible() ? "fa-check-square" : "fa-square");

        //build label
        let label = document.createElement("span");
        let title = document.createElement("span");

        title.textContent = " " + column.getDefinition().title;

        label.appendChild(icon);
        label.appendChild(title);

        //create menu item
        menu.push({
            label: label,
            action: function (e) {
                //prevent menu closing
                e.stopPropagation();

                //toggle current column visibility
                column.toggle();

                //change menu item icon
                if (column.isVisible()) {
                    icon.classList.remove("fa-square");
                    icon.classList.add("fa-check-square");
                } else {
                    icon.classList.remove("fa-check-square");
                    icon.classList.add("fa-square");
                }
            }
        });
    }

    return menu;
};