(function () {
  // fillOptionsSelect("delimiterSelect", [
  //   { text: 'virgule', value: ',' },
  //   { text: 'point-virgule', value: ';' },
  //   { text: 'tabulation', value: '\t' },
  //   { text: 'barre verticale', value: '|' }
  // ],
  //   { prop: "fontSize", value: ".8rem" }
  // );
  // fillOptionsSelect("encodingSelect", encodingsSingleBytes,
  //   { prop: "fontSize", value: ".8rem" }
  // )
  // dayjs.locale('fr');
  // console.log('date', dayjs().format('YYYY-MM-DD'));
})();

// *** FETCH *** //
document.getElementById('loadData').addEventListener("click", () => {
  // const urlSIDEP = "https://www.data.gouv.fr/fr/datasets/r/426bab53-e3f5-4c6a-9d54-dba4442b3dbc";
  // const urlLegislatives2022 = "https://www.data.gouv.fr/fr/datasets/r/c79af2f3-1733-4df3-be37-caebacbc1321";

  let url = document.getElementById('urlInput').value;
  if (!url) return;

  url = url ? url.trim() : '';
  document.getElementById('loader-div').classList.remove('hidden');

  fetchData(url, "url")
    .then(rawdata => {
      let divId = "tables-container";
      // launchTable(createTable(rawdata, divId));
    });

}, false);

document.getElementById('urlInput').onkeydown = (e) => {
  if (!e.target.value) return;
  // console.log(e.code);

  if (e.code === "Enter") {
    let url = e.target.value;
    url = url ? url.trim() : '';
    document.getElementById('loader-div').classList.remove('hidden');

    fetchData(url, "url")
      .then(rawdata => {
        let divId = "tables-container";
        // launchTable(createTable(rawdata, divId));
      });
  }
}

const fetchData = async (url, from) => {
  // developer.mozilla.org/en-US/docs/Web/API/AbortController/abort
  const controller = new AbortController();
  const signal = controller.signal;
  const output = document.getElementById('info-fetch');

  const loadBtn = document.getElementById('loadData');
  loadBtn.disabled = true;

  // // si besoin
  // setTimeout(() => controller.abort(), 5000);

  try {
    const res = await fetch(url, { signal });
    console.log('status', res.status);

    if (res.status === 200) {
      let rawdata = await getDataFrom(res, from);
      controller.abort();
      loadBtn.disabled = false;
      return rawdata;
    } else {
      console.log(`Error code ${res.status}`);
      output.textContent = `Code erreur ${res.status}`;
      controller.abort();
    }
  }
  catch (err) {
    if (err.name !== 'AbortError') {
      output.textContent = `Code erreur ${res.status}`;
      controller.abort();
    }
  }
}

// *** FILEREADER *** //

fileInput.onchange = async function (e) {
  document.getElementById('loader-div').classList.remove('hidden');
  let files = [...fileInput.files];
  // files = files.sort((a, b) => b.size - a.size);
  let file = files[0];
  let size = formatBytes(file.size);
  // console.log(size);
  let divId = "tables-container";
  let data = await readerFile(file);
  console.log('started !', data);
  // launchTable(createTable(data, divId));
}

const readerFile = (file) => {
  // console.log(file);
  return new Promise(function (resolve, reject) {
    const output = document.getElementById('info-fetch');
    const reader = new FileReader();

    reader.onload = (event) => {
      // console.log(event.target.result);
      getDataFrom(event.target.result, "file")
        .then(data => {
          console.log(data);
          if (data) {
            resolve(data);
          } else {
            document.getElementById('loader-div').classList.add('hidden');
            output.textContent = "Oups ! Le programme rencontré une erreur...";
          }

        });
    };
    reader.onloadend = (event) => {
      console.log("loadend !");
    };
    reader.onerror = (error) => {
      console.log("erreur : " + error);
      document.getElementById('loader-div').classList.add('hidden');
      reject(error);
    };
    reader.onabort = (event) => {
      console.log("erreur : " + event);
      document.getElementById('loader-div').classList.add('hidden');
    };
    reader.readAsArrayBuffer(file);
    // reader.readAsText(file);
  });
}

// *** COMMONS DATA PROCESS *** //
const getDataFrom = async (res, from) => {
  const output = document.getElementById('info-fetch');
  const verifCheckbox = document.getElementById('verif-csv');

  const gridsContainer = document.getElementById('grids-container');
  const gridHeaders = document.getElementById('grid-headers');

  const radioDelimiterBtns = document.querySelectorAll('input[name="radiodelimiter"]');
  let delimiterInput = document.getElementById('delimiterInput');
  let encodingSelect = document.getElementById('encodingSelect');

  // default papaparse "," & "utf-8"
  let delimiter = ',',
    encoding = "utf-8";

  // for papaparse
  let config = { delimiter: delimiter };
  // console.log(config, encoding);

  // let delimiterValue = document.getElementById('delimiterSelect').value;
  // let select = document.getElementById('encodingSelect');
  // let encoding = select ? select.options[select.selectedIndex].value : "utf-8";

  const buffer = from === "url" ? await res.arrayBuffer() : res;

  let bufferSize = buffer.byteLength;

  const textString = (buffer, encoding) => {
    let dataView = new DataView(buffer);
    let decoder = new TextDecoder(encoding); // default "utf-8"

    let decodedString = decoder.decode(dataView);
    return decodedString;
  };

  //www.papaparse.com/docs#config
  let array = Papa.parse(textString(buffer), config).data;
  let arrayHeaders = array[0].map(el => " ");
  let arrayReal = [];

  if (!verifCheckbox.checked) {
    document.getElementById('info-container').classList.add('hidden');
    launchTable(createTable(jsonFromArray(array), "tables-container"));
    document.getElementById('loader-div').classList.add('hidden');
    return true;
  }

  const firstLine =
    /\n/.test(textString(buffer)) ? textString(buffer).split('\n')[0] :
      /\r/.test(textString(buffer)) ? textString(buffer).split('\r')[0] :
        textString(buffer);

  // console.log(firstLine);

  const comma = (/\,(?=\S)/gi).test(firstLine);
  const testSemicolon = /\;(?=\S)/gmi.test(firstLine);
  const testTab = /\t/gmi.test(firstLine);

  console.log('test delimiter', comma, testSemicolon, testTab);

  radioDelimiterBtns.forEach(btn =>
    btn.checked =
    (getDelimiter(btn.value) == ',') ? true :
      (testSemicolon && getDelimiter(btn.value) == ';') ? true :
        (testTab && getDelimiter(btn.value) == '\t') ? true
          : false
  )
  // console.log('test delimiter : ', testTab, testSemicolon);

  // console.log(jschardet.detect(textString(buffer, encoding), { minimumThreshold: 0 }));

  // console.log(arrayHeaders);
  createTableHeaders(["", "ligne"], array.slice(0, 10), "headers-config", "grid-headers")
    .then(headersTable => {
      document.getElementById('loader-div').classList.add('hidden');
      document.getElementById('toolbar-load').classList.add('hidden');
      document.getElementById('info-container').classList.add('hidden');

      if (headersTable) {
        gridsContainer.classList.remove('hidden');
        fillOptionsSelect("encodingSelect", encodingsSingleBytes,
          { prop: "fontSize", value: ".8rem" }
        )

        delimiterInput.onkeydown = (e) => {
          if (delimiterInput.value) {
            radioDelimiterBtns.forEach(btn => btn.checked = false)
            config.delimiter = getDelimiter(delimiterInput.value);
            encoding = encodingSelect.value;
            array = Papa.parse(textString(buffer, encoding), config).data;
            createTableHeaders(
              ["", "ligne"],
              array.slice(0, 10),
              "headers-config",
              "grid-headers",
              config.delimiter
            ).then(headersTable => {
              console.log(headersTable);
            })
          }
        }
        radioDelimiterBtns.forEach(btn => {
          btn.onclick = e => {
            delimiterInput.value = "";
            config.delimiter = getDelimiter(btn.value);
            encoding = encodingSelect.value;
            console.log(config, encoding);

            array = Papa.parse(textString(buffer, encoding), config).data;
            createTableHeaders(
              ["", "ligne"],
              array.slice(0, 10),
              "headers-config",
              "grid-headers",
              config.delimiter
            ).then(headersTable => {
              console.log(headersTable);
            })
          }
        })

        encodingSelect.addEventListener('change', function (e) {
          radioDelimiterBtns.forEach(btn => {
            if (delimiterInput.value) {
              config.delimiter = getDelimiter(delimiterInput.value);
              encoding = this.value;
              btn.checked = false;
            } else if (btn.checked) {
              config.delimiter = getDelimiter(btn.value);
              encoding = this.value;
            }
          });
          array = Papa.parse(textString(buffer, encoding), config).data;
          console.log(config, encoding, array[0]);
          // gridsContainer.innerHTML = '';
          createTableHeaders(
            ["", "ligne"],
            array.slice(0, 10),
            "headers-config",
            "grid-headers",
            config.delimiter
          ).then(headersTable => {
            console.log(headersTable);
          })
        })

        gridHeaders.addEventListener('click', (e) => {
          console.log(e.target.className);
          radioDelimiterBtns.forEach(btn => {
            if (btn.checked) {
              config.delimiter = getDelimiter(btn.value);
            }
          });
          encoding = encodingSelect.value;
          console.log(config, encoding);

          if (e.target.className === "checkboxHeader") {
            let index = e.target.dataset.index;
            array = Papa.parse(textString(buffer, encoding), config).data;
            arrayReal = array.slice(index, array.length);

            gridsContainer.classList.add('hidden');
            document.getElementById('loader-div').classList.remove('hidden');

            console.log(formatBytes(bufferSize), bufferSize, bufferSize >= 30000000);

            let innerContent = (bufferSize <= 20000000) ?
              "Traitement des données, veuillez patienter..." :
              `Oups ! Le fichier paraît très volumineux et ça peut prendre beaucoup (trop) de temps.
             Il est possible que votre navigateur plante avant de pouvoir afficher les données...`;

            document.getElementById('loader-text').innerText = innerContent;

            setTimeout(() => {
              document.getElementById('info-container').classList.add('hidden');
              launchTable(createTable(jsonFromArray(arrayReal), "tables-container"));
              document.getElementById('loader-div').classList.add('hidden');
            }, 10);
          }
        });
      }
    })

  // problem with header if contain dot... 
  // let jsonFromCsv = (delimiter !== "t") ? d3.dsvFormat(delimiter).parse(decodedString) : d3.dsvFormat("\t").parse(decodedString);

  return true; //jsonFromArray(array); //csvToJson; //jsonFromCsv;
};

const getDelimiter = (value) => {
  let delimiterInput = document.getElementById('delimiterInput');
  let delimiter =
    (value === ',') ? ',' :
      (value === ';') ? ';' :
        (value === 't') ? '\t' :
          delimiterInput.value.trim();

  return delimiter;
}
const radioDelimiter = function () {
  const radioButtons = document.querySelectorAll('input[name="radiodelimiter"]');
  console.log(radioButtons);
}

const jsonFromArray = (array) => {
  const headers = array[0];
  return array.slice(1).map(row => {
    let obj = {};
    row.forEach((cell, i) => {
      let header = /\./gi.test(headers[i]) ? headers[i].replace(/\./gi, '') : headers[i];
      obj[header] = cell;
    })
    return obj;
  })
}

// à conserver mais probleme de quotes ?
// voir éventuellement fast-csv librairie
const csvIntoJson = (csv, separator) => {
  let [headers, ...rows] = csv.split('\n');
  headers = headers.split(separator);
  // console.log(headers);
  rows = rows.map(row => row.split(separator));

  return rows.reduce((jsonArray, row) => {
    // console.log(row);
    const item = row.reduce((item, value, index) => {
      let header = /\./gi.test(headers[index]) ? headers[index].replace(/\./gi, '') : headers[index];
      return { ...item, [header]: value };
    }, {});
    return jsonArray.concat(item);
  }, []);
};