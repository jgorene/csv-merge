const launchTable = (table) => {
    if (!table) {
        return;
    }

    window.onresize = function () {
        table.setHeight(Math.round(window.innerHeight) - 60);
    };

    let headers = [];

    table.on("tableBuilt", (e) => {
        // document.getElementById('spinnerLoad-span').classList.replace("hidden", "inline");
        document.getElementById('toolbar-load').classList.add("hidden");
        document.getElementById('toolbar-table').classList.remove("hidden");
        let data = table.getData();

        // table.replaceData(data).then(data => {
        //     console.log(data);
        // });

        table.setGroupBy(true);

        headers = table.getColumnLayout().map(column => column.field);
        let headersOptions = headers.filter(header => header != "Fiche").map(item => {
            return { text: item, value: item };
        })


        // table.addColumn({title:"Fiche", field:"fiche"}, true);

        fillOptionsSelect("filter-field", headersOptions);
        fillOptionsSelect("merge-select", headersOptions);
        fillOptionsSelect("filter-type", [
            { text: "mot-clé", value: "like" },
            { text: "égal à", value: "=" },
            { text: "inférieur à", value: "<" },
            { text: "inf. ou égal à", value: "<=" },
            { text: "supérieur à", value: ">" },
            { text: "sup. ou égal à", value: "<=" },
            { text: "différent de", value: "!=" },
        ]);
    });
    // table.on("rowClick", function(e, row){
    //     console.log(e, row);
    // });
    table.on("cellClick", function (e, cell) {
        let col = cell.getColumn();
        let colClass = col.getDefinition().cssClass;
        let colPos = /col_/gi.test(colClass) ? colClass.match(/col_\d+/)[0].split('col_')[1] : "";

        if (colPos && +colPos === 0) {
            // console.log(colClass, /col_/gi.test(colClass), colPos);
            let rowData = Object.entries(cell.getRow().getData());
            let dataExport = rowData.filter(arr => arr[0] !== "Fiche").filter(arr => arr[0] !== "id");
            createTableExport(["entête", "valeur"], dataExport, "pvtTable");
        }
    });

    //deselect row on "deselect all" button click
    document.getElementById('deselectAll-rows').onclick = () => table.deselectRow();

    document.getElementById('hide-col').onclick = () => {
        let columnNames = [];
        // console.log(table.getColumnLayout());
        table.getColumnLayout().forEach(column => {
            if (column.visible)
                columnNames.push(column.field);
        });
        createTableColumns(["", "colonnes"], columnNames, "pvtTable", table, "hide");
        columnNames = null;
    }

    document.getElementById('show-col').onclick = function () {
        let columnNames = [];
        table.getColumnLayout().forEach(column => {
            if (!column.visible)
                columnNames.push(column.field);
        });
        createTableColumns(["", "colonnes"], columnNames, "pvtTable", table, "show");
        columnNames = null;
    }

    // document.getElementById('filtersHeader-clear').onclick = function (e) {
    //     table.clearHeaderFilter();
    // }

    document.getElementById('download-csv').onclick = function (e) {
        table.download("csv", "export.csv"); // , {delimiter:","}
    }

    //trigger download of data.xlsx file
    document.getElementById("download-xlsx").addEventListener("click", function () {
        table.download("xlsx", "data.xlsx", { sheetName: "My Data" });
    });

    //trigger download of data.pdf file
    document.getElementById("download-pdf").addEventListener("click", function () {
        table.download("pdf", "data.pdf", {
            orientation: "portrait", //set page orientation to portrait
            title: "Example Report", //add title to report
        });
    });

    // *** GROUP *** //

    document.getElementById('groupBy-btn').onclick = function (e) {
        // console.log(this.textContent);
        if (this.textContent === "Grouper par") {
            let columnNames = [];
            // console.log(table.getColumnLayout());
            table.getColumnLayout().forEach(column => {
                if (column.field !== "Fiche")
                    columnNames.push(column.field);
            });
            console.log(columnNames);
            createTableColumns(["", "colonnes"], columnNames, "pvtTable", table, "group");
            columnNames = null;
        } else {
            table.setGroupBy("");
            document.getElementById('groupBy-btn').textContent = "Grouper par";
            document.getElementById('groupBy-icon').innerHTML = '<i class="fas fa-lock-open"></i>'
        }
        // // PM
        //     // document.getElementById('tables-container').classList.remove("hidden");
        //     // d3.select('#chart-div').selectAll("svg").remove();
        //     // document.getElementById('chart-div').classList.add("hidden");
        //     // document.getElementById('chart-btn').dataset.value = "diagram";
        //     // document.getElementById('chart-btn').innerHTML = '<i class="fas fa-project-diagram"></i>';
        //     // document.getElementById('chart-btn').classList.add("hidden");
    }

    // *** Merge *** //

    document.getElementById('merge-btn').onclick = function (e) {
        let select = document.getElementById('merge-select');
        let selectedValue = select.options[select.selectedIndex].value;
        // console.log(selectedValue, table.getData(true));

        let dataExport = mergeData(headers, table.getData(true), selectedValue);

        // console.log(dataExport);
        exportToCsv(dataExport, "merged");
    }

    function mergeData(headers, jsonData, selected) {
        // console.log(jsonData);
        let dataByColumn = (arr, col) => {
            // console.log(arr, col);
            let column = [],
                value;
            for (let i = 0, lgi = arr.length; i < lgi; i++) {
                value = arr[i][col];
                column.push(value);
            }
            return filter_array(column); // voir avec _.compact
        }

        //d3.csvParse(Papa.unparse(data));
        // console.log(jsonData);

        // also allow to get unique headers
        let headersTemplate = {};
        headers = headers
        .filter(header => header != "Fiche")
        .filter(header => header != "id")
        // .filter(header => header != selected)
        headers.forEach((el, i) => {
            headersTemplate[el] = "";
        });

        let merged = [];

        // observablehq.com/@d3/d3-group
        let nestedData = d3.rollup(
            jsonData,
            v => {
                let arr = v.map(obj => [].concat.call([], Object.values(obj)));
                arr.unshift(headers);
                return {
                    arr: arr
                };
            },
            d => d[selected]
        );
        // console.log(nestedData);

        // changement avec la V7
        nestedData.forEach((value, key) => {
            let values = value.arr,
                obj = {};
            headers.forEach((k, i) => {
                obj[k] =
                    [...new Set(dataByColumn(values.slice(1, values.length), i))]
                        .join(',').replace(/,/g, ' # ').split();
            });
            merged.push(Object.assign({}, headersTemplate, obj));
        })

        let dataMerged = d3.csvParseRows(Papa.unparse(merged));

        return dataMerged;
    }


    // *** FILTERS *** //

    function updateFilter() {
        let filter = document.getElementById("filter-field").value; //$("#filter-field").val() === "function" ? customFilter : $("#filter-field").val();
        console.log(filter);

        if (document.getElementById("filter-field").value === "function") {
            document.getElementById("filter-type").disabled = true;
            document.getElementById("filter-value").disabled = true;
        } else {
            document.getElementById("filter-type").disabled = false;
            document.getElementById("filter-value").disabled = false;
        }
        // console.log(filter, $("#filter-type").val(), $("#filter-value").val());
        table.setFilter(
            filter,
            document.getElementById("filter-type").value,
            document.getElementById("filter-value").value
        );
    };

    //Update filters on value change
    document.getElementById("filter-field").onchange = updateFilter;
    document.getElementById("filter-type").onchange = updateFilter;
    document.getElementById("filter-value").onkeyup = updateFilter;
    //Clear filters on "Clear Filters" button click

    document.getElementById("filter-clear").onclick = () => {
        document.getElementById("filter-field").value = headers[0];
        document.getElementById("filter-type").value = "like";
        document.getElementById("filter-value").value = "";

        table.clearFilter();
    };
    return true;
} // ==== fin de launchTable