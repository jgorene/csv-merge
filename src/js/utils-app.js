const frFR = { //French language definition
    "columns": {
        "name": "Nom",
        "progress": "Progression",
        "gender": "Genre",
        "rating": "Évaluation",
        "col": "Couleur",
        "dob": "Date de Naissance",
    },
    "pagination": {
        "first": "<<",
        "first_title": "Première Page",
        "last": ">>",
        "last_title": "Dernière Page",
        "prev": "<",
        "prev_title": "Page Précédente",
        "next": ">",
        "next_title": "Page Suivante",
        "all": "Toutes",
        "page_size": "Page par",
        "counter": {
            "showing": " ",
            "of": "sur",
            "rows": "lignes",
            "pages": "pages"
        }
    },
};

// tableau principal => instance de Tabulator
const createTable = (rawdata, divId) => {
    const container = document.getElementById(divId);
    while (container.firstChild) container.removeChild(container.lastChild);

    let headers = rawdata.columns ? rawdata.columns : Object.keys(rawdata[0]); //rawdata.columns;
    headers.forEach(header => header !== '' ? header : '?');
    headers.unshift('Fiche');

    let tabledata = JSON.parse(JSON.stringify(rawdata));

    tabledata.forEach((el, i) => {
        el.id = 'row_' + i;
        el.Fiche = "";
    })
    // fontawesome.com/search?q=download&m=free
    let printIcon = () => "<i class='fa fa-download'></i>";

    let columns = headers.map((header, i) => {

        const isNumeric = (val) => parseFloat(val) == val;
        const checkDate = (string) => {
            let testDate = /\d{2,4}[\/|\-]{1}\d{2}[\/|\-]{1}\d{2,4}/gi.test(string);
            let testDate_fr = /\d{2,4}[\/|\s]{1}\d{2}[\/|\s]{1}\d{2,4}/gi.test(string);
            let dateArr, dateString;
            if (testDate_fr) {
                dateArr = string.split(/\/|\s/);
                dateString = dateArr[2] + '-' + dateArr[1] + '-' + dateArr[0];
            }
            return dateString;
        }

        let classByColumn = function (arr, col) {
            let column = [],
                value;
            for (let i = 1, lgi = arr.length; i < lgi; i++) {
                // value = $.isNumeric(arr[i][col]) ? "number" : testDate.test(arr[i][col])  ? "date" : "string";
                value = isNumeric(arr[i][col]) ? "number" : "string"; //"number" : "string";

                checkDate(arr[i][col]);

                column.push(value);
            }

            return [...new Set(filter_array(column))]; // voir avec _.compact
        }
        let columnType = classByColumn(tabledata, header);
        // console.log(columnType);

        let columnClass = columnType.length > 0 ? "number" : "string"; //[...new Set(columnType)];
        // console.log(columnClass);

        let headerContext = function (e, column) {
            e.preventDefault();
        };

        let config = {
            cssClass: 'col_' + i,
            title: header,
            field: header,
            headerTooltip: true,
            // headerMenu: headerMenu,
            tooltip: true,
            // headerContext: headerContext,
        }

        if (header === "Fiche") {
            console.log(header);
            config.formatter = printIcon;
            config.hozAlign = "center";
            config.maxWidth = "50px";
            config.headerSort = false;
        }

        return config;
    });

    let footerContent = '<div class="footerInfo"> ';
    // footerContent += '<a type="button" href="https://github.com/olifolkerd/tabulator" target="_blank" style="margin-right: 2em; padding: 2px 5px; font-weight: 900">Tabulator</a>'
    footerContent += 'lignes: <span id="rowsTotal" style="font-weight: 900">' + tabledata.length + '</span>';
    footerContent += '<span style="margin-left: 1em">colonnes: </span><span id="columnsTotal" style="font-weight: 900">' + headers.length + '</span>';
    footerContent += '<div style="margin-left: 2em;" class="inline">';
    // footerContent += '<span>lignes: </span><span id="rowsCount" style="font-weight: 900"></span> (filtrée.s)';
    footerContent += '<span style="margin-left: 2em">sélection: </span><span id="rowSelected" style="font-weight: 900"></span> (ligne.s)';
    footerContent += '</div></div>';

    let optionsTable = {
        selectable: true, //make rows selectable
        height: Math.round(window.innerHeight) - 60,
        renderHorizontal: "virtual",
        layout: "fitColumns",
        movableColumns: true,
        resizableColumnFit: true,
        // footerElement: footerContent,
        placeholder: "Aucune donnée disponible",
        groupToggleElement: "header",
        pagination: "local",
        paginationSize: 100,
        paginationSizeSelector: [10, 30, 50, 100],
        movableColumns: true,
        paginationCounter: "rows",
        langs: {
            "fr-fr": frFR
        },
        locale: "fr-fr",
        // locale: true,
        data: tabledata,
        // autoColumns: true, //create columns from data field names
        columns: columns,
        // =====
        groupStartOpen: function (value, count, data, group) {
            // console.log(value, count, data, group);
            return false; //all groups with more than three rows start open, any with three or less start closed
        },
        // groupContext: function (e, group) {
        //     e.preventDefault();
        //     let inputGroup = document.getElementById('groupBy-input').value;
        //     inputGroup = inputGroup.split('>').map(el => el.trim());
        //     let rowsData = [];
        //     let subGroups = group.getSubGroups();
        //     let groupElement = group.getElement();
        //     if (subGroups.length === 0) {
        //         let rows = group.getRows().forEach(row => {
        //             rowsData.push(row.getData());
        //         });
        //         let parentGroup = inputGroup;
        //         let key = group.getKey();
        //         rowsData = d3.csvParseRows(Papa.unparse(rowsData));
        //         exportCSVDefault(rowsData, inputGroup.join('_') + "_" + key);
        //     } else {
        //         groupElement.classList.add("shaker");
        //         setTimeout(() => {
        //             groupElement.classList.remove("shaker");
        //         }, 400);
        //     }
        // }
    };
    // console.log(headers, tabledata);

    //initialize table
    let table = new Tabulator(container, optionsTable);

    return table;
};

const groupByCol = (table, fields) => {
    if (fields) {
        table.setGroupBy(fields);
        // document.getElementById('chart-btn').classList.replace("hidden", "inline");
        // this.innerHTML = '<i class="fas fa-lock"></i>';
    }
}

// *** export csv, txt **** //

const exportToCsv = function (rows, filename) {
    let processRow = function (row) {
        let finalVal = '';
        for (let j = 0; j < row.length; j++) {
            let innerValue = (row[j] === null || row[j] === undefined) ? '' : row[j].toString();
            if (row[j] instanceof Date) {
                innerValue = row[j].toLocaleString();
            };
            if (typeof row[j] == "number") {
                innerValue = row[j].toLocaleString();
            };
            let result = innerValue.replace(/"/g, '""');
            if (result.search(/("|,|\n)/g) >= 0)
                result = '"' + result + '"';
            if (j > 0)
                finalVal += ',';
            finalVal += result;
        }
        return finalVal + '\n';
    };

    let csvFile = "\ufeff";
    //    let csvFile = '';
    for (let i = 0; i < rows.length; i++) {
        csvFile += processRow(rows[i]);
    }

    let blob = new Blob([csvFile], {
        type: 'text/csv; charset=utf-8;'
    });
    if (navigator.msSaveBlob) { // IE 10+
        navigator.msSaveBlob(blob, filename);
    } else {
        let link = document.createElement("a");
        if (link.download !== undefined) { // feature detection
            // Browsers that support HTML5 download attribute
            let url = URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", filename);
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
}

const exportToTxt = function (text, filename) {
    let textFile = "\ufeff";
    textFile += text;
    // //    let csvFile = '';
    // for (let i = 0; i < rows.length; i++) {
    //     csvFile += processRow(rows[i]);
    // }

    let blob = new Blob([textFile], {
        type: 'text/plain; charset=utf-8;'
    });
    if (navigator.msSaveBlob) { // IE 10+
        navigator.msSaveBlob(blob, filename);
    } else {
        let link = document.createElement("a");
        if (link.download !== undefined) { // feature detection
            // Browsers that support HTML5 download attribute
            let url = URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", filename);
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
}

// *** HELPERS *** //

const fillOptionsSelect = (selectId, options, style) => {
    // console.log(selectId, options, style);
    let select = document.getElementById(selectId);
    while (select.options.length)
        select.options.remove(0);

    if (options && options.length) {
        let opt = document.createElement("option");
        for (let i = 0; i < options.length; i++) {
            // console.log(options[i]);
            let option = document.createElement("option");
            option.text = options[i].text;
            option.value = options[i].value;
            if (style) {
                option.style[style.prop] = style.value;
            }
            // options.dataset.icon = ""
            select.add(option, i);
        }
    }
}

// Remove null, 0, blank, false, undefined and NaN values from an array
// https://www.w3resource.com/javascript-exercises/javascript-array-exercise-24.php
const filter_array = (test_array) => {
    let index = -1,
        arr_length = test_array ? test_array.length : 0,
        resIndex = -1,
        result = [];
    while (++index < arr_length) {
        let value = test_array[index];
        if (value) {
            result[++resIndex] = value;
        }
    }
    return result;
}

const formatBytes = (bytes, decimals = 2) => {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

//help.libreoffice.org/latest/fr/text/shared/guide/csv_params.html?DbPAR=SHARED
//list legacy single-byte encodings
const encodingsSingleBytes = [
    {text: 'Unicode (UTF-8)', value: 'utf-8'},
    {text: 'ISO-8859-1 (Latin 1)', value: 'ISO-8859-1'},
    {text: 'ISO-8859-3 (Latin 3)', value: 'ISO-8859-3'},
    {text: 'Windows-1252 (WinLatin 1)', value: 'Windows-1252'}
]
